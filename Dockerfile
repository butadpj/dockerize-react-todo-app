FROM node:14-alpine
WORKDIR /app
COPY . /app
RUN yarn install --production
CMD ["yarn", "start"]